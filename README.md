Project Title:
-------------
This project enable create portofolio automatically containing different asset type according input parameter. After it compute the different rentability of the diferent portofolio created.

Authors:
--------
* LACROIX Lionel


Getting Started:
----------------
These instructions will get you a copy of the  project up and running on your local machine for development and testing purposes. 
See prerequisites and installing for notes on how to deploy the project on a live system.


Documentation:
--------------
The documentation about the library is not available.

Prerequisites:
---------------
CMake version 2.8 or more later

Installation:
-----------
on Linux:
sudo apt-get install libcurl4-openssl-dev rapidJson-dev gtest


Steps to compile and install the library:
Create a directory named build in the root of the solution. 
In this directory :
```bash
$ cmake ..
$ make 
```

Last version :
-------------
1.0.0
