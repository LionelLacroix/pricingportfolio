#include "PolygonDataAccess.h"
#include "Url.h"
#include <fstream>
#define MARKET_DATA_WEBSITE "PolygonWebsite.json"



PolygonDataAccess::PolygonDataAccess()
{
    std::ifstream websiteUrlParamFile(MARKET_DATA_WEBSITE);
    if(websiteUrlParamFile.is_open())
    {
        std::stringstream buffer;
        buffer << websiteUrlParamFile.rdbuf();
        m_jsonMaketDataParams.Parse(buffer.str().c_str());
        if(!m_jsonMaketDataParams.IsObject())
            std::cerr<<"The "<<MARKET_DATA_WEBSITE<<" file is incorect"<<std::endl;
    }
    else
        std::cerr<<"Could not open "<<MARKET_DATA_WEBSITE<<" file "<<std::endl;

}

int PolygonDataAccess::getLimitRequest()
{
    return m_jsonMaketDataParams["limit"].GetInt();
}

std::string PolygonDataAccess::createURLFromTicker(const std::string & ticker,const boost::gregorian::date& dayFrom,
                                        const boost::gregorian::date & dayTo)
{
    std::string url;

    std::string adress = m_jsonMaketDataParams["adress"].GetString();
     adress +="v2/aggs/ticker/"+ticker+"/range/1/day/"+boost::gregorian::to_simple_string(dayFrom)+"/"+boost::gregorian::to_simple_string(dayTo);
    std::string name = m_jsonMaketDataParams["name"].GetString();
    const rapidjson::Value& jsonOptions = m_jsonMaketDataParams["options"];
    assert(jsonOptions.IsArray());
    Options options;
    options.resize(jsonOptions.Size());
    for(rapidjson::SizeType i =0;i <jsonOptions.Size(); i++ )
    {
        options[i].first = jsonOptions[i]["name"].GetString();
        options[i].second = jsonOptions[i]["name"].GetString();
    }
    UrlMaketData urlMaketData(name,adress,m_jsonMaketDataParams["apikey"].GetString(),options);

    return url;
}

bool PolygonDataAccess::processData(const std::string& response, const boost::gregorian::date& dateFrom,
                         const boost::gregorian::date & dateTo,std::vector<DailyData> & datas)
{
     rapidjson::Document jsonResponse;
     jsonResponse.Parse(response.c_str());
     const rapidjson::Value& jsonData =  jsonResponse["results"];
     boost::gregorian::date_duration dur = dateTo - dateFrom;
     if(jsonResponse["status"] != "OK")
        return false;

     if(jsonResponse["count"].GetInt() !=  dur.days())
     {
         std::cout<<" "<<std::endl;
         return false;
     }


     const rapidjson::Value& jsonFields =  m_jsonMaketDataParams["fields"];
     datas.resize(jsonData.Size());
     boost::gregorian::day_iterator iterDate = dateFrom;
     for(rapidjson::SizeType i =0;i <jsonData.Size(); i++ )
     {
         for(rapidjson::SizeType j =0;j <jsonFields.Size(); j++ )
         {
             const rapidjson::Value& jsonValue = jsonData[i];
             const boost::gregorian::date idate(iterDate->year(),iterDate->month(),iterDate->day());
             datas[i].fillDailyData(idate,
                     jsonValue[jsonFields[CLOSED_PRICE]].GetDouble(),
                     jsonValue[jsonFields[HIGHTEST_PRICE]].GetDouble(),
                     jsonValue[jsonFields[LOWEST_PRICE]].GetDouble(),
                     jsonValue[jsonFields[NUMBER]].GetInt(),
                     jsonValue[jsonFields[OPEN_PRICE]].GetDouble(),
                     jsonValue[jsonFields[TRADING_VOLUME]].GetDouble(),
                     jsonValue[jsonFields[TRADING_VOLUME]].GetDouble());
         }
     }
    return true;
}
