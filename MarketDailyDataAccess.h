#include <string>
#include <vector>
#include <memory>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include "HTTPRequest.h"
#include "MarketDataStructure.h"
#include "SQLiteHelperDailyData.h"
#include "DataAccess.h"

class MarketDailyDataAccess
{
public:
    explicit MarketDailyDataAccess();


    /**brief
    **/
    bool updateData(const std::string& ticker);

    ~MarketDailyDataAccess() =default;

private:
    boost::gregorian::date getLastDateUpdated(const std::string& ticker);

private:
    DailyData m_dailyData;
    HTTPRequest m_httpRequest;
    SQLiteHelperDailyData m_sqliteHelper;
    std::vector<std::unique_ptr<DataAccess>> m_dataAccess;


};




