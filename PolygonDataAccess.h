#pragma once
#include "DataAccess.h"
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>


class PolygonDataAccess :  public DataAccess
{
    public:
        explicit PolygonDataAccess();

        virtual std::string createURLFromTicker(const std::string & ticker,const boost::gregorian::date& dateFrom,
                                        const boost::gregorian::date & dateTo) override;
        virtual std::string getDataAccessType() override { return "Polygon";}
        virtual int getLimitRequest() override;
        virtual bool processData(const std::string& response, const boost::gregorian::date& dateFrom,
                         const boost::gregorian::date & dateTo,std::vector<DailyData> & datas) override;
        virtual ~PolygonDataAccess() =default;
    private:
        rapidjson::Document m_jsonMaketDataParams;

};

