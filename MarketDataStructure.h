#pragma once
#include <string>
#include <vector>
#include <boost/date_time/gregorian/gregorian.hpp>
#include "Utils.h"
#define DATE 0
#define CLOSED_PRICE 1
#define HIGHTEST_PRICE 2
#define LOWEST_PRICE 3
#define NUMBER 4
#define OPEN_PRICE 5
#define TRADING_VOLUME 6
#define AVERAGE_PRICE 7
#define DAILY_MARKET_SIZE 8

static std::vector<std::pair<std::string,std::string>>  dailyDataStruct
{
    {"DATE","DATETIME"},
    {"CLOSED_PRICE","REAL"},
    {"HIGHTEST_PRICE","REAL"},
    {"LOWEST_PRICE","REAL"},
    {"NUMBER","INT"},
    {"OPEN_PRICE","REAL"},
    {"TRADING_VOLUME","INT"},
    {"AVERAGE_PRICE","REAL"}
};


struct DailyData
{
    void fillDailyData(const boost::gregorian::date& date_, double closed_price_ ,
              double hightest_price_, double lowest_price_,
              int number_, double open_price_, double trading_volume_,
              double average_price_)
    {
        date = date_;
        closed_price = closed_price_;
        hightest_price = hightest_price_;
        lowest_price = lowest_price_;
        number = number_;
        open_price = open_price_;
        trading_volume = trading_volume_;
        average_price = average_price_;
    }

    bool operator==(DailyData &other)
        {
        return (date == other.date &&
        closed_price == other.closed_price &&
        hightest_price == other.hightest_price &&
        lowest_price == other.lowest_price &&
        number == other.number &&
        open_price == other.open_price &&
        trading_volume == other.trading_volume &&
        average_price == other.average_price);
    }
    DailyData() =default;
    boost::gregorian::date date;
    double closed_price;
    double hightest_price;
    double lowest_price;
    int number;
    double open_price;
    double trading_volume;
    double average_price;
};

