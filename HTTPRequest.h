#include <string>
#include <curl/curl.h>

class HTTPRequest{

public:
    explicit HTTPRequest();
    ~HTTPRequest();
    std::string & getResponseContent(){return m_responseContent;}
    bool request(const std::string & url);

private:
    static size_t internal_curl_write_callback(char* src, size_t size, size_t nmemb, void* userp);
    bool RequestCurlInit(const std::string & url);
    bool RequestCurlPerform();

private:
    std::string m_url;
    std::string m_responseContent;
    CURL* m_curl;
    long m_HTTPErrorCode;
};
