#include <string>
#include <unordered_map>
#include <vector>
#include "MarketDataStructure.h"


class sqlite3;

class SQLiteHelperDailyData
{
public:
   explicit SQLiteHelperDailyData();
   bool initMarketData(const std::string & database);
   bool createTable(const std::string & ticker);
   bool updateData(const std::string & ticker,
                   const std::vector<DailyData>& Data);
   bool isTableExist(const std::string & ticker);
   bool deleteTable(const std::string & ticker);

   void getMissingDateInDataBase(const std::string & ticker,
                                 const boost::gregorian::date& dateFrom,
                                 const boost::gregorian::date& dateTo,
                                 std::vector<boost::gregorian::date>& missingDate );
   bool getData(const std::string & ticker,const boost::gregorian::date& dateFrom,
                const boost::gregorian::date& dateTo, std::vector<DailyData>& Data);

   bool getLastDateUpdated(const std::string& ticker, boost::gregorian::date & date);

 ~SQLiteHelperDailyData()=default;
private:
   enum CallBackType
   {
         Data,
         Check,
         None
   };
   static int internalSqliteCallback(void* data, int argc, char** argv, char** azColName);
   sqlite3 * m_db;
   static std::vector<DailyData> m_buffer;
   static bool isValid;
};
