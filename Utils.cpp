#include "Utils.h"
#include <iostream>
#include <cstdlib>
#include <filesystem>

#define ENV_VARIABLE "PRICER_PATH"

namespace Utils {

std::string getEnvPath()
{
	const char* env = std::getenv(ENV_VARIABLE);
	std::string envVar;
    if (env == 0)
        std::cerr<<"Environment variable " + std::string(ENV_VARIABLE) + " is not defined"<<std::endl;
    else
        envVar = env;
    if(!std::filesystem::exists(envVar))
        std::cerr<<"Environment variable " + std::string(ENV_VARIABLE) + " does not exist"<<std::endl;
    return envVar;
}

std::string getConfigPath()
{
	std::string configPath = getEnvPath();
	if (!configPath.empty())
		configPath += "/config/";
	if (!std::filesystem::exists(configPath))
	{
		std::filesystem::create_directory(configPath);
		std::cout << "Has created config directory" << std::endl;
	}
	return configPath;
}

std::string getResourcesPath()
{
	std::string resourcesPath = getEnvPath();
	if (!resourcesPath.empty())
		resourcesPath += "/resources/";
	if (!std::filesystem::exists(resourcesPath))
        std::cerr<< "ressource path " + resourcesPath + " does not exist"<<std::endl;

	return resourcesPath;
}


std::vector<std::string> splitString(const std::string &str, const std::string & delim)
{
    std::vector<std::string> tokens;

    std::string::size_type start = 0;
    std::string::size_type end = 0;

    while ((end = str.find(delim, start)) != std::string::npos)
    {
        tokens.push_back(str.substr(start, end - start));
        start = end + 1;
    }

    tokens.push_back(str.substr(start));

    return tokens;
}

};
