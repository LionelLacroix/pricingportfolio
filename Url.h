#include <string>
#include <vector>

typedef  std::vector<std::pair<std::string , std::string>> Options;


class UrlMaketData
{
public:
    explicit UrlMaketData() = default;
    explicit UrlMaketData(const std::string& name, const std::string& webSite,
                 const std::string& APIKey, const Options& options): m_name(name),
      m_webSite(webSite),
      m_APIKey(APIKey),
      m_options(options)
    {
        m_url = webSite;
        m_url +="?";
        for(const auto & option: options)
        {
            m_url+=option.first+"="+option.second;
            m_url+="&";
        }
        m_url+="apiKey="+APIKey;
    }
    void setName(const std::string& name){m_name = name;}
    void setWebsite(const std::string website){ m_webSite = website;}
    void setOptions(const Options& options){m_options = options;}
    void setAPIKey(const std::string& APIKey) { m_APIKey = APIKey;}
    std::string getName() {return m_name;}
    std::string getUrl(){ return m_url;};
private:
    std::string m_url;
    std::string m_name;
    std::string m_webSite;
    std::string m_APIKey;
    Options m_options;
};
