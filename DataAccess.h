#pragma once
#include "MarketDataStructure.h"


class DataAccess
{
public:
    DataAccess() = default;
    virtual ~DataAccess();

    virtual std::string createURLFromTicker(const std::string & ticker,const boost::gregorian::date& dayFrom,
                                    const boost::gregorian::date & dayTo) = 0;
    virtual std::string getDataAccessType() = 0;
    virtual int getLimitRequest() = 0;
    virtual bool processData(const std::string& response, const boost::gregorian::date& dayFrom,
                     const boost::gregorian::date & dayTo, std::vector<DailyData> & datas)  = 0;
};
