#include "MarketDailyDataAccess.h"
#include "PolygonDataAccess.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include "Url.h"
#define MARKET_DATA_WEBSITE "website.json"
#define DATABASE_PATH  "dailyDatabase.db"
#define POLYGON 1
#define NB_DAY_PER_YEAR 360


MarketDailyDataAccess::MarketDailyDataAccess()
{

    m_sqliteHelper.initMarketData(Utils::getConfigPath()+"/"+DATABASE_PATH);
    m_dataAccess.push_back(std::unique_ptr<DataAccess>(new PolygonDataAccess()));
}

bool MarketDailyDataAccess::updateData(const std::string& ticker)
{

    //m_sqliteHelper.getMissingDateInDataBase();
     std::string response;

     boost::gregorian::date dateFrom = getLastDateUpdated(ticker);
     boost::gregorian::date dateTo(boost::gregorian::day_clock::local_day());
     boost::gregorian::date_duration dur = dateTo - dateFrom;
     if( dur.days()==0)
     {
         std::cout<<"ticker already updated"<<std::endl;
         return true;
     }

    std::vector<DailyData> datas;
    for(auto & dataAccess : m_dataAccess)
    {
        std::vector<int> durations;
        int nbRequest = 1;
        if( dur.days() > dataAccess->getLimitRequest())
        {
            nbRequest = dur.days()/dataAccess->getLimitRequest();
            int nbEltLastRequest = dur.days() - nbRequest*dataAccess->getLimitRequest();
            durations.resize(nbRequest,dataAccess->getLimitRequest());
            if(nbEltLastRequest>0)
            {
                durations.push_back(nbEltLastRequest);
                nbRequest++;
            }

        }
        boost::gregorian::date iDateTo = dateTo;
        for( int i=0; i < nbRequest; ++i)
        {
            if(!durations.empty())
                iDateTo = dateFrom + boost::gregorian::date_duration(durations[i]);

            std::string url  = dataAccess->createURLFromTicker(ticker,dateFrom, iDateTo);
            bool retRequest = m_httpRequest.request(url);
            if(!retRequest)
            {
                std::cerr<<"Invalid request "+ dataAccess->getDataAccessType()+ " website.";
                break;
            }
            response = m_httpRequest.getResponseContent();
            if (!response.empty() && dataAccess->processData(response,dateFrom,dateTo, datas))
                dateFrom= iDateTo;
            else
                break;
        }
        if(iDateTo == dateTo)
            break;
    }
    return true;
}


boost::gregorian::date MarketDailyDataAccess::getLastDateUpdated(const std::string& ticker)
{

    boost::gregorian::date current_date(boost::gregorian::day_clock::local_day());
    boost::gregorian::date  dateFrom = current_date- boost::gregorian::date_duration(NB_DAY_PER_YEAR);
    if(m_sqliteHelper.isTableExist(ticker))
    {
        boost::gregorian::date lastDate;
        bool ret = m_sqliteHelper.getLastDateUpdated(ticker,lastDate);
        if(ret && lastDate > dateFrom)
            return lastDate;
    }
    return dateFrom;


}



