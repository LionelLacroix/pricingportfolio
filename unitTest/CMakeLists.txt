enable_testing()

find_package(GTest REQUIRED)
add_executable( unitTest
main_Test.cpp
SQLiteHelperDailyData_Test.cpp
HTTPRequest_Test.cpp)

target_link_libraries(unitTest
    PricerUtils
    GTest::GTest
    GTest::Main
    )

include(GoogleTest)
gtest_discover_tests(unitTest)
