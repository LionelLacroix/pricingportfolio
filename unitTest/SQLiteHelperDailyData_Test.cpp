#include "../SQLiteHelperDailyData.h"
#include "../MarketDataStructure.h"
#include "../Utils.h"
#include <gtest/gtest.h>

TEST(SQLiteHelperDalyDataTest, working_request)
{
SQLiteHelperDailyData databaseDailyData;
std::string configPath = Utils::getConfigPath();
if(configPath.empty())
    ASSERT_TRUE(false);

std::string dataBasePath = configPath+"/dailyDatabaseTest.db";

databaseDailyData.initMarketData(dataBasePath);
std::vector<DailyData> datasIn;
datasIn.resize(2);
boost::gregorian::date date= boost::gregorian::from_simple_string("2023-06-18");
datasIn[0].fillDailyData(date,54.3,55.7,52.1,100,55.0,7.0,53.5);
date= boost::gregorian::from_simple_string("2023-06-17");
datasIn[1].fillDailyData(date,84.3,85.7,82.1,100,85.0,7.0,84.5);
databaseDailyData.updateData("TestTicker",datasIn);
std::vector<DailyData> datasOut;
//databaseDailyData.getData("TestTicker",18062023,17062023, datasOut);

//ASSERT_TRUE(response.size());
}
