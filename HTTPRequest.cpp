#include "HTTPRequest.h"
#include <iostream>

HTTPRequest::HTTPRequest():m_curl(nullptr)
{}

size_t HTTPRequest::internal_curl_write_callback(char* src, size_t size, size_t nmemb, void* userp)
{
    const size_t totalSize = size*nmemb;
    HTTPRequest* req = (HTTPRequest*)(userp);
    if(!req)
    {
        std::cerr<<"Error: userp is null write callback"<<std::endl;
        return 0;
    }
    req->getResponseContent().append(static_cast<const char*>(src),totalSize);
    return totalSize;
}

//DPCJS8H7J5JQCAYF. Key alpha ventage
bool HTTPRequest::RequestCurlInit( const std::string & url )
{

    m_curl = curl_easy_init();
    if(!m_curl)
    {
        std::cerr<<"Error : curl easy init failed"<<std::endl;
        return false;
    }

    CURLcode res;
    res = curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    if(CURLE_OK != res)
    {
        std::cerr<<"Error : "<<curl_easy_strerror(res)<<std::endl;
        return false;
    }
    res = curl_easy_setopt(m_curl, CURLOPT_FOLLOWLOCATION, 1L);
    if(CURLE_OK != res)
    {
        std::cerr<<"Error : "<<curl_easy_strerror(res)<<std::endl;
        return false;
    }

    res = curl_easy_setopt(m_curl, CURLOPT_TIMEOUT,35L); //Timeout 30 s to start
    if(CURLE_OK != res)
    {
        std::cerr<<"Error : "<<curl_easy_strerror(res)<<std::endl;
        return false;
    }

    res = curl_easy_setopt(m_curl, CURLOPT_WRITEDATA,this);
    if(CURLE_OK != res)
    {
        std::cerr<<"Error : "<<curl_easy_strerror(res)<<std::endl;
        return false;
    }

    res = curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, HTTPRequest::internal_curl_write_callback);
    if(CURLE_OK != res)
    {
        std::cerr<<"Error : "<<curl_easy_strerror(res)<<std::endl;
        return false;
    }
    return true;
}

bool HTTPRequest::RequestCurlPerform()
{
    do
    {
        m_responseContent = "";
        CURLcode res = curl_easy_perform(m_curl);
        if(res == CURLE_OK)
        {
            curl_easy_getinfo(m_curl,CURLINFO_RESPONSE_CODE, &m_HTTPErrorCode);
            //std::cout<<"http code "<<
        }
        if(m_responseContent.size())
        {
            //json code to parse
            return true;
        }
    }
    while(false);
    return true;
}

bool HTTPRequest::request(const std::string &url)
{
    if(!RequestCurlInit(url))
    {
        std::cerr<<"An error is occurred at the inititialisation of the request";
        return false;

    }
return RequestCurlPerform();
}

HTTPRequest::~HTTPRequest()
{
    curl_easy_cleanup(m_curl);
}


