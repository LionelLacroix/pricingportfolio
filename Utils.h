#include <string>
#include <vector>

namespace Utils {

std::string getConfigPath();

std::string getResourcesPath();

std::vector<std::string> splitString(const std::string &str, const std::string & delim) ;

}

