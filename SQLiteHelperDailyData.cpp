#include <sqlite3.h>
#include <iostream>
#include "SQLiteHelperDailyData.h"
#include <unordered_map>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <locale>

std::vector<DailyData> SQLiteHelperDailyData::m_buffer;
bool SQLiteHelperDailyData::isValid = false;

SQLiteHelperDailyData::SQLiteHelperDailyData():m_db(nullptr)
{

}


bool SQLiteHelperDailyData::initMarketData(const std::string & databasePath)
{
    if(!std::filesystem::exists(std::filesystem::path(databasePath)))
    {
        std::ofstream file(databasePath);
        file.close();
    }

    int error = sqlite3_open(databasePath.c_str(), &m_db);
    if (error) {
        std::cerr << "Error open DB " << sqlite3_errmsg(m_db) << std::endl;
        return false;
    }

     std::cout << "Opened Database Successfully!" << std::endl;
     return true;

}

int SQLiteHelperDailyData::internalSqliteCallback(void* data, int argc, char** argv, char** )
{

    CallBackType type = CallBackType::None;
    if(data)
       type = *(CallBackType*)(data);
    switch(type)
    {
         case CallBackType::Data :
         {
            DailyData dailyData;
            if(argc < DAILY_MARKET_SIZE)
                return 1;
            if(argv[DATE])
                dailyData.date = boost::gregorian::from_simple_string(argv[DATE]);
            dailyData.open_price = argv[OPEN_PRICE] ?  atof(argv[OPEN_PRICE]) : 0;
            dailyData.closed_price = argv[CLOSED_PRICE]?  atof(argv[CLOSED_PRICE]) :0;
            dailyData.average_price = argv[AVERAGE_PRICE]? atof(argv[AVERAGE_PRICE]) :0;
            dailyData.lowest_price = argv[LOWEST_PRICE]? atof(argv[LOWEST_PRICE]) :0;
            dailyData.hightest_price = argv[HIGHTEST_PRICE]? atof(argv[HIGHTEST_PRICE]) :0;
            dailyData.number = argv[NUMBER]? atoi(argv[NUMBER]): 0;
            dailyData.trading_volume = argv[TRADING_VOLUME]? atoi(argv[TRADING_VOLUME]):0;
            m_buffer.push_back(dailyData);
            break;
         }

          case CallBackType::Check:
          {
             if(argc < 1)
                 return 1;
             isValid = argv[0];
             break;
          }

          case CallBackType::None:
          {
             return 1;
          }
    }
    return 0;
}

bool SQLiteHelperDailyData::deleteTable(const std::string & ticker)
{
    std::string createTable = "DROP TABLE IF EXISTS "+ticker;
    char* messageError;
    int error = sqlite3_exec(m_db, createTable.c_str(), NULL, 0, &messageError);
    if (error != SQLITE_OK) {
        std::cerr << "An Error occured Could not update Table" +ticker << std::endl;
        sqlite3_free(messageError);
        return false;
    }
    return true;
}

bool SQLiteHelperDailyData::createTable(const std::string & ticker)
{

    //create table if not exist
    std::string createTable = "CREATE TABLE IF NOT EXISTS "+ticker+"(";
    for(const auto & datastruct : dailyDataStruct)
        createTable+=datastruct.first+" "+datastruct.second+" ";
    createTable+=");";
    char* messaggeError;
    int error = sqlite3_exec(m_db, createTable.c_str(), NULL, 0, &messaggeError);

    if (error != SQLITE_OK) {
        std::cerr<<createTable<<std::endl;
        std::cerr << "Error Create Table" << std::endl;
        sqlite3_free(messaggeError);
        return false;
    }
    return true;
}


bool SQLiteHelperDailyData::updateData(const std::string & ticker,
                                       const std::vector<DailyData>& Datas )
{
    if(!createTable(ticker))
        return false;

    boost::gregorian::date_facet df("%Y-%m-%d");
    // set your formatting
    std::ostringstream ssDate;
    ssDate.imbue(std::locale(ssDate.getloc(), &df));

    //add data in the ticker table
    std::string insertSql;
    for(const auto & data : Datas)
    {
        ssDate<<data.date;
        insertSql+="INSERT INTO "+ticker+" VALUES("+ssDate.str()+
                ", "+std::to_string(data.closed_price)+", "+std::to_string(data.hightest_price)+\
                ", "+std::to_string(data.lowest_price)+", "+std::to_string(data.number)+", "+std::to_string(data.open_price)+\
                " ,"+std::to_string(data.trading_volume)+" ,"+std::to_string(data.average_price)+");";
    }
     char* messageError;
     int error = sqlite3_exec(m_db, insertSql.c_str(), NULL, 0, &messageError);

    if (error != SQLITE_OK) {
        std::cerr << "An Error occured Could not update Table" +ticker << std::endl;
        sqlite3_free(messageError);
        return false;
    }
    return true;
}

bool SQLiteHelperDailyData::isTableExist(const std::string & ticker)
{

    char* messageError;
    void * dataType = (void*)(CallBackType::Check);

    std::string query = "SELECT EXISTS (SELECT name FROM sqlite_schema WHERE type='table' AND";
        query+="name='"+ticker+"');";
    int error = sqlite3_exec(m_db, query.c_str(), SQLiteHelperDailyData::internalSqliteCallback, dataType, &messageError);
    if (error != SQLITE_OK) {
        std::cerr << "An Error occured Could not update Table" +ticker << std::endl;
        sqlite3_free(messageError);
        return false;
    }
    return isValid;
}

bool SQLiteHelperDailyData::getData(const std::string & ticker,
                                    const boost::gregorian::date& dateFrom,
                                    const boost::gregorian::date& dateTo,
                                        std::vector<DailyData>& data)
{
    char* messageError;
    void * dataType = (void*)(CallBackType::Data);
    if(!dateFrom.is_not_a_date()|| !dateTo.is_not_a_date())
    {
        std::cerr<<"The date entered are not  correct"<<std::endl;
        return false;
    }
    boost::gregorian::date_facet df("%Y-%m-%d");
    // set your formatting
    std::ostringstream ssDate;
    ssDate.imbue(std::locale(ssDate.getloc(), &df));
    std::string query = "SELECT * FROM "+ticker+" WHERE DATE BETWEEN '"+
            ssDate.str()+"' AND '"+ssDate.str()+"' ORDER BY "+ dailyDataStruct[DATE].first+" ;";
    int error = sqlite3_exec(m_db, query.c_str(), SQLiteHelperDailyData::internalSqliteCallback, dataType, &messageError);
    if (error != SQLITE_OK)
    {
        std::cerr << "An Error occured. Could not get data in table" +ticker << std::endl;
        sqlite3_free(messageError);
        return false;
    }
    data = m_buffer;
    m_buffer.clear();
    return true;
}

bool SQLiteHelperDailyData::getLastDateUpdated(const std::string& ticker, boost::gregorian::date & date)
{
    std::string query  = "SELECT max(DATE) FROM "+ticker;
    char* messageError;
    void * dataType = (void*)(CallBackType::Data);
    int error = sqlite3_exec(m_db, query.c_str(), SQLiteHelperDailyData::internalSqliteCallback, dataType, &messageError);
    if (error != SQLITE_OK)
    {
        std::cerr << "An Error occured Could not update Table" +ticker << std::endl;
        sqlite3_free(messageError);
        return false;
    }
    if(m_buffer.empty())
        return false;
    date = m_buffer[0].date;
    return true;
}

void SQLiteHelperDailyData::getMissingDateInDataBase(const std::string & ticker,
                              const boost::gregorian::date& dateFrom,
                              const boost::gregorian::date& dateTo,
                              std::vector<boost::gregorian::date>& missingDate )
{

    std::vector<DailyData> datas;
    getData(ticker, dateFrom,dateTo, datas);
    int index = 0;
    for(boost::gregorian::day_iterator iter = dateFrom;
        iter!=dateTo; ++iter)
    {
        const auto & dateInSQL = datas[index].date;
        const boost::gregorian::date idate(iter->year(),iter->month(),iter->day());
        if(dateInSQL < idate)
        {
            if(idate.day_of_week()!= boost::gregorian::Saturday ||
               idate.day_of_week()!= boost::gregorian::Sunday)
            {
                missingDate.push_back(dateInSQL);
                index++;
            }
        }
    }
}



